#!/bin/bash

set -o allexport; source config/.env; set +o allexport

# Install the local CA in the system trust store
mkcert -install 
