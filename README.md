# Extism-IDE

This project is a helper to create a Multipass VM with all the tools needed to develop for Extism.

> 👋 all the tests are done on a Mac (arm architecture). If you are on another OS, you will have to adapt the scripts.

## Requirements

- Install [mkcert](https://mkcert.org/)
- Initialize mkcert with `initialize-mkcert.sh`

## Setup

- update this file: `./config/.env`
- run `./generate-vm.sh`

## Finalize

- The file `vm.hosts.config` has been generated. You need to add its content to your `/etc/hosts` file:
- Then you can access the IDE with this URL: https://extism-web-ide.local:9090/?folder=/home/ubuntu/workspaces

> 👋 the default password is `admin`

## Add your GitLab credentials

- Go to the IDE: https://extism-web-ide.local:9090/?folder=/home/ubuntu/workspaces
- And follow the `workspaces/README.md` instructions

## Helpers

- Stop the VM: `./stop-vm.sh`
- Start the VM: `./start-vm-ide.sh`