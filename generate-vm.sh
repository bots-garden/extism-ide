#!/bin/bash

set -o allexport; source config/.env; set +o allexport

echo "🔑 creating certificate for ${VM_DOMAIN}"

mkcert ${VM_DOMAIN} "*.${VM_DOMAIN}" 

cp ${VM_DOMAIN}+1-key.pem certs/${VM_DOMAIN}.key
cp ${VM_DOMAIN}+1.pem certs/${VM_DOMAIN}.cert


echo "🖥️ creating ${VM_NAME}"

multipass launch docker --name ${VM_NAME} \
--cpus ${VM_CPUS} \
--memory ${VM_MEM} \
--disk ${VM_DISK} \
--cloud-init ./vm.cloud-init.yaml

# share the directories 
multipass mount certs ${VM_NAME}:certs
#multipass mount scripts ${VM_NAME}:scripts
multipass mount workspaces ${VM_NAME}:workspaces

VM_IP=$(multipass info ${VM_NAME} | grep IPv4 | awk '{print $2}')

multipass info ${VM_NAME}

multipass exec ${VM_NAME} -- sudo -- sh -c "echo \"${VM_IP} ${VM_DOMAIN}\" >> /etc/hosts"

echo "${VM_IP} ${VM_DOMAIN}" > config/vm.hosts.config

# ------------------------------
# Configurations
# ------------------------------
BAT_VERSION="0.22.1"
BAT_ARCH="arm64"

GOLANG_VERSION="1.20"
GOLANG_OS="linux"
GOLANG_ARCH="arm64"
TINYGO_VERSION="0.28.1"
TINYGO_ARCH="arm64"

#multipass --verbose exec ${VM_NAME} -- sudo -- bash <<EOF
multipass --verbose exec ${VM_NAME} -- bash <<EOF
sudo snap install task --classic

# ------------------------------
# Install requirements
# ------------------------------
sudo apt-get update
sudo apt install build-essential -y
sudo apt install unzip -y
sudo apt install zip -y
sudo apt install openssl -y
sudo apt install libssl-dev -y
sudo apt install git -y
sudo apt install pkg-config -y

# ------------------------------
# Install Tools
# ------------------------------
 
# Install Bat
wget https://github.com/sharkdp/bat/releases/download/v${BAT_VERSION}/bat_${BAT_VERSION}_${BAT_ARCH}.deb
sudo dpkg -i bat_${BAT_VERSION}_${BAT_ARCH}.deb
rm bat_${BAT_VERSION}_${BAT_ARCH}.deb      

# Install Exa
sudo apt-get install exa -y

# Install Hey
sudo apt-get update
sudo apt-get -y install hey

# Install httpie
sudo snap install httpie

# Install OhMyBash
bash -c "\$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"


# -----------------------
# Install GoLang
# -----------------------

echo "Installing Go & TinyGo"

wget https://go.dev/dl/go${GOLANG_VERSION}.${GOLANG_OS}-${GOLANG_ARCH}.tar.gz

sudo rm -rf /usr/local/go 
sudo tar -C /usr/local -xzf go${GOLANG_VERSION}.${GOLANG_OS}-${GOLANG_ARCH}.tar.gz

echo "" >> ~/.bashrc
echo 'export GOLANG_HOME="/usr/local/go"' >> ~/.bashrc
echo 'export PATH="\$GOLANG_HOME/bin:\$PATH"'  >> ~/.bashrc
source ~/.bashrc

rm go${GOLANG_VERSION}.${GOLANG_OS}-${GOLANG_ARCH}.tar.gz               

# -----------------------
# Install TinyGo
# -----------------------
wget https://github.com/tinygo-org/tinygo/releases/download/v${TINYGO_VERSION}/tinygo_${TINYGO_VERSION}_${TINYGO_ARCH}.deb
sudo dpkg -i tinygo_${TINYGO_VERSION}_${TINYGO_ARCH}.deb
rm tinygo_${TINYGO_VERSION}_${TINYGO_ARCH}.deb

export GOLANG_HOME="/usr/local/go"
export PATH="$GOLANG_HOME/bin:$PATH"

go version
go install -v golang.org/x/tools/gopls@latest
go install -v github.com/ramya-rao-a/go-outline@latest
go install -v github.com/stamblerre/gocode@v1.0.0

# -----------------------
# Install NodeJS
# -----------------------

curl -sL https://deb.nodesource.com/setup_19.x | sudo -E bash -
sudo apt-get install -y nodejs

# -----------------------
# Install Rust support
# -----------------------
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh -s -- -y

echo 'export CARGO_HOME="~/.cargo"' >> ~/.bashrc
echo 'export PATH=\$CARGO_HOME/bin:\$PATH' >> ~/.bashrc

source ~/.cargo/env
source ~/.bashrc

# Add wasm & wasi targets
curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
rustup target add wasm32-wasi
rustup target add wasm32-unknown-unknown

# -----------------------
# Install Wasm Bindgen
# -----------------------
cargo install -f wasm-bindgen-cli

rustup component add rust-analysis --toolchain stable-aarch64-unknown-linux-gnu 
rustup component add rust-src --toolchain stable-aarch64-unknown-linux-gnu 
rustup component add rls --toolchain stable-aarch64-unknown-linux-gnu

# -----------------------
# Install Wasi Runtimes
# -----------------------

curl -sSf https://raw.githubusercontent.com/WasmEdge/WasmEdge/master/utils/install.sh | bash

source ${HOME}/.wasmedge/env

curl https://get.wasmer.io -sSfL | sh
source ${HOME}/.wasmer/wasmer.sh

curl https://wasmtime.dev/install.sh -sSf | bash
source ${HOME}/.bashrc

# -----------------------
# Install Extism
# -----------------------
sudo apt-get update -y
sudo apt-get install -y pkg-config

sudo apt install python3-pip -y
pip3 install poetry
pip3 install git+https://github.com/extism/cli

echo "export EXTISM_HOME=\"\$HOME/.local\"" >> ${HOME}/.bashrc
echo "export PATH=\"\$EXTISM_HOME/bin:\$PATH\"" >> ${HOME}/.bashrc

source ${HOME}/.bashrc

#extism install latest

extism --prefix=/usr/local install latest
pip3 install extism


# -----------------------
# Install Extism JS PDK
# -----------------------
#curl -O https://raw.githubusercontent.com/extism/js-pdk/main/install.sh
#sh install.sh

export TAG="v0.5.0"
export ARCH="aarch64"
export  OS="linux"
curl -L -O "https://github.com/extism/js-pdk/releases/download/$TAG/extism-js-$ARCH-$OS-$TAG.gz"
gunzip extism-js*.gz
sudo mv extism-js-* /usr/local/bin/extism-js
chmod +x /usr/local/bin/extism-js

EOF


# First setup and start code-server
multipass --verbose exec ${VM_NAME} -- bash <<EOF
echo "💾 Installing Code Server"
curl -fsSL https://code-server.dev/install.sh | sh

mkdir -p ~/.config/code-server

echo "bind-addr: ${VM_IP}:${CODE_SERVER_PORT}" > ~/.config/code-server/config.yaml
echo "auth: password" >> ~/.config/code-server/config.yaml
echo "password: ${CODE_SERVER_PWD}" >> ~/.config/code-server/config.yaml
echo "cert: certs/${VM_DOMAIN}.cert" >> ~/.config/code-server/config.yaml
echo "cert-key: certs/${VM_DOMAIN}.key" >> ~/.config/code-server/config.yaml

#echo "cert: false" >> ~/.config/code-server/config.yaml

cat ~/.config/code-server/config.yaml

echo "🧩 Install some Code Server Extensions"
code-server --install-extension wesbos.theme-cobalt2
code-server --install-extension PKief.material-icon-theme
code-server --install-extension PKief.material-product-icons
code-server --install-extension golang.go
code-server --install-extension task.vscode-task
code-server --install-extension DaltonMenezes.aura-theme
code-server --install-extension codeium.codeium
code-server --install-extension aaron-bond.better-comments
code-server --install-extension GitHub.github-vscode-theme
code-server --install-extension huytd.github-light-monochrome
code-server --install-extension cweijan.vscode-redis-client

code-server --install-extension rust-lang.rust-analyzer

echo "🚀 Start Code Server"

code-server &

echo "🌍 https://${VM_DOMAIN}:${CODE_SERVER_PORT}/?folder=/home/ubuntu/workspaces"

EOF

echo "+-----------------------------------------------+"
echo "🖐️ update your /etc/hosts file with:"
cat config/vm.hosts.config
echo "+-----------------------------------------------+"

