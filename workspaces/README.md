# Add your GitLab credentials 

- Rename `Taskfile.template.yml` to `Taskfile.yml`
- Update the values:
    ```yaml
    env:
      EMAIL: "your_e_mail"
      HANDLE: "your_gitlab_handle"
    ```
- Run `task generate-ssh-key` and follow the instructions
- Copy the result (the key) to your GitLab.com user settings
- Run `task git-config`

## Your projects

Now you can clone your projects in the `workspaces` directory