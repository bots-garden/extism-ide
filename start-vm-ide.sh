#!/bin/bash
set -o allexport; source config/.env; set +o allexport

multipass start ${VM_NAME}

VM_IP=$(multipass info ${VM_NAME} | grep IPv4 | awk '{print $2}')

multipass --verbose exec ${VM_NAME} -- bash <<EOF
echo "🚀 Start Code Server"

code-server &

echo "🌍 https://${VM_DOMAIN}:${CODE_SERVER_PORT}/?folder=/home/ubuntu/workspaces"
EOF


